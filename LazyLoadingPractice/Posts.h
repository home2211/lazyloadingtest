//
//  Posts.h
//  LazyLoadingPractice
//
//  Created by Saloni Agarwal on 4/9/16.
//  Copyright © 2016 Saloni_App. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface Posts : MTLModel<MTLJSONSerializing>
@property (nonatomic,strong)NSArray *postsArray;


@end
