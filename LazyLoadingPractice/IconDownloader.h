//
//  IconDownloader.h
//  LazyLoadingPractice
//
//  Created by Saloni Agarwal on 4/9/16.
//  Copyright © 2016 Saloni_App. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Post;

@interface IconDownloader : NSObject
@property (nonatomic, copy) void (^completionHandler)(void);
@property (nonatomic,strong)Post *post;
- (void)startDownload;
- (void)cancelDownload;

@end
