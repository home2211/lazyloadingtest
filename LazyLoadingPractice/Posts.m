//
//  Posts.m
//  LazyLoadingPractice
//
//  Created by Saloni Agarwal on 4/9/16.
//  Copyright © 2016 Saloni_App. All rights reserved.
//

#import "Posts.h"
#import "Post.h"
#import <Mantle/Mantle.h>

@implementation Posts
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    
    return @{
             @"postsArray" : @"posts"
             };
}

+(NSValueTransformer*)postsArrayJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Post class]];
}
@end
