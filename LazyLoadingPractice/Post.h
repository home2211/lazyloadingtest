//
//  Post.h
//  LazyLoadingPractice
//
//  Created by Saloni Agarwal on 4/9/16.
//  Copyright © 2016 Saloni_App. All rights reserved.
//

#import <Mantle/Mantle.h>
#import <UIKit/UIKit.h>
@interface Post : MTLModel <MTLJSONSerializing>
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSString *thumbnail;
@property(nonatomic,strong)UIImage *thumbnailImage;
@end
