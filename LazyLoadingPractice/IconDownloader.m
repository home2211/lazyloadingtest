//
//  IconDownloader.m
//  LazyLoadingPractice
//
//  Created by Saloni Agarwal on 4/9/16.
//  Copyright © 2016 Saloni_App. All rights reserved.
//

#import "IconDownloader.h"
#import "Post.h"

#import <UIKit/UIKit.h>
#define kAppIconSize 48

@interface IconDownloader ()

@property (nonatomic, strong) NSURLSessionDataTask *sessionTask;

@end
@implementation IconDownloader
// -------------------------------------------------------------------------------
//	startDownload
// -------------------------------------------------------------------------------
- (void)startDownload
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:self.post.thumbnail]];
    
    // create an session data task to obtain and download the app icon
    _sessionTask = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                       
       // in case we want to know the response status code
       //NSInteger HTTPStatusCode = [(NSHTTPURLResponse *)response statusCode];
       
       if (error != nil)
       {
           if ([error code] == NSURLErrorAppTransportSecurityRequiresSecureConnection)
           {
               // if you get error NSURLErrorAppTransportSecurityRequiresSecureConnection (-1022),
               // then your Info.plist has not been properly configured to match the target server.
               //
               abort();
           }
       }
       
       [[NSOperationQueue mainQueue] addOperationWithBlock: ^{
           
           // Set appIcon and clear temporary data/image
           UIImage *image = [[UIImage alloc] initWithData:data];
           
           if (image.size.width != kAppIconSize || image.size.height != kAppIconSize)
           {
               CGSize itemSize = CGSizeMake(kAppIconSize, kAppIconSize);
               UIGraphicsBeginImageContextWithOptions(itemSize, NO, 0.0f);
               CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
               [image drawInRect:imageRect];
               self.post.thumbnailImage = UIGraphicsGetImageFromCurrentImageContext();
               UIGraphicsEndImageContext();
           }
           else
           {
               self.post.thumbnailImage = image;
           }
           
           // call our completion handler to tell our client that our icon is ready for display
           if (self.completionHandler != nil)
           {
               self.completionHandler();
           }
       }];
   }];
    
    [self.sessionTask resume];
}

// -------------------------------------------------------------------------------
//	cancelDownload
// -------------------------------------------------------------------------------
- (void)cancelDownload
{
    [self.sessionTask cancel];
    _sessionTask = nil;
}

@end
