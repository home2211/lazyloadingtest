//
//  AppDelegate.h
//  LazyLoadingPractice
//
//  Created by Saloni Agarwal on 4/9/16.
//  Copyright © 2016 Saloni_App. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

