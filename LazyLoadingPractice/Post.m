//
//  Post.m
//  LazyLoadingPractice
//
//  Created by Saloni Agarwal on 4/9/16.
//  Copyright © 2016 Saloni_App. All rights reserved.
//

#import "Post.h"

@implementation Post
+ (NSDictionary*)JSONKeyPathsByPropertyKey {
    return @{
             @"title":@"title",
             @"thumbnail":@"thumbnail"
             };
}
@end
